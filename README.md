# dotfiles bootstrap #

use this script to bootrap a dotfiles installation

### How do I get set up? ###

go to your home folder and execute bootstrap

``bash -c "`wget --no-check-certificate -qO- https://bitbucket.org/chrizz-api/dotfiles/raw/master/dotfiles`" NULL init``

list available packages

`dotfiles list`

enable some (or your selection)

`dotfiles install bash vim tmux misc bin`

remove installed (only removes repository and restores backup yet)

`dotfiles remove bash`